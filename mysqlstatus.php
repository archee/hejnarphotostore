<title>Tech Support-System Specs</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
h1 {
	color: #B56224;
}
h2 {
	color: #D67732;
}
h3 {
	color: #D67630;
}
.red {
	color: #FF0000;
	font-weight: bolder;
}
.green {color: #009900;
	font-weight: bolder;
}
-->
</style>
</head>
<body>

<?
function containsInfo($value) {
	if( isset($value) && ($value != '') )
		return true;
	else
		return false;
}

  if (file_exists('includes/application_top.php')) {
      require('includes/application_top.php');
	  };
if( containsInfo($_REQUEST['server']) && containsInfo($_REQUEST['database']) && containsInfo($_REQUEST['username']) && containsInfo($_REQUEST['password']) ) {
	
?>

<center><a href="#serverinfo2">SERVER INFORMATION</a><br><a href="#phpinfo">PHP INFORMATION</a><br>
<H1>This is a set of simple scripts to let you see the configuration of your server. </h1>
<br><br><br></center>
<?
// Report the Zen Cart System info, if available.
  if (file_exists('includes/application_top.php')) {
?>

<h1>Server Configuration Details</h1>
<h2>ZEN Cart System Info:</h2><ul>
<h3>From APPLICATION_TOP.PHP</h3><ul>
<li><strong>Version: </strong><? echo PROJECT_VERSION_NAME; ?></li><br />
<li><strong>Version Major: </strong><? echo PROJECT_VERSION_MAJOR; ?></li><br />
<li><strong>Version Minor: </strong><? echo PROJECT_VERSION_MINOR; ?></li></ul>

<h3>Settings from Zen Cart Database:</h3><ul>
<li><strong>Installed Payment Modules: </strong><? echo MODULE_PAYMENT_INSTALLED; ?></li><br />
<li><strong>Installed Order Total Modules: </strong><? echo MODULE_ORDER_TOTAL_INSTALLED; ?></li><br />
<li><strong>Installed Shipping Modules: </strong><? echo MODULE_SHIPPING_INSTALLED; ?></li><br />
<li><strong>Default Currency: </strong><? echo DEFAULT_CURRENCY; ?></li><br />
<li><strong>Default Language: </strong><? echo DEFAULT_LANGUAGE; ?></li><br />
<li><strong>Enable Downloads: </strong><? echo DOWNLOAD_ENABLED; ?></li><br />
<li><strong>Enable GZip Compression: </strong><? echo GZIP_LEVEL; ?></li><br />
<li><strong>Admin Demo Status: </strong><? echo ADMIN_DEMO; ?></li>
</ul></ul>
<?
  };
?>
<a name="serverinfo2"></a>
<h2>Server Info</h2>Return to the top of the page <a href="#top" title="Back to top"><font style="font-family: Webdings; font-size: 15px; text-decoration: none">5</font></a>
<ul>
<li><strong>Webserver: </strong><?=getenv("SERVER_SOFTWARE")?></li><br /><br />
<li><strong>REGISTER_GLOBALS</strong>= <? echo ini_get("register_globals"); ?></li><br />
<li><strong>SAFE_MODE</strong>= 
<? if (ini_get("safe_mode")) {
   echo "<span class='red'>ON</span>";
   } else{
   echo "OFF";
   }; ?></li>
<br /><li>
<?php
$link = mysql_connect(trim($_REQUEST['server']), trim($_REQUEST['username']), trim($_REQUEST['password']));
if (!$link) {
   die('Could not connect: ' . mysql_error());
}
$db_list = mysql_list_dbs($link);

while ($row = mysql_fetch_object($db_list)) {
echo "<b>MySql Info for: </b>";
     echo $row->Database . "<br>";
}

$version =mysql_get_server_info($link);
echo "<b>Mysql version:</b> $version <br>";
echo "<b>MySql VARIABLES:</b><br>";
$result = mysql_query('SHOW VARIABLES', $link);
while ($row = mysql_fetch_assoc($result)) {
   echo '<b>' . $row['Variable_name'] . ' </b>= ' . $row['Value'] . "<br>";
   
}


mysql_close($link);



?> </li>
<br /><li><strong>PHP version: </strong>
<? if (phpversion()=="4.1.2") {
   echo "<span class='red'>".phpversion()." {You SHOULD upgrade this!}</span>";
   } else {
   echo phpversion();
   }; ?></li>

<br />
<li><strong>HTTP_HOST =</strong>  <?php echo $_SERVER['HTTP_HOST'];?></li><br />
<li><strong>PATH_TRANSLATED =</strong>  <?php  echo $_SERVER['PATH_TRANSLATED']; ?></li><br />
<li><strong>SCRIPT_FILENAME =</strong>  <?php echo $_SERVER['SCRIPT_FILENAME'];?></li><br />
<li><strong>SCRIPT_NAME =</strong>  <?php echo $_SERVER['SCRIPT_NAME']; ?></li><br />
<li><strong>SESSION.SAVE_PATH = </strong><span class="green"><?php echo ini_get("session.save_path"); ?></span></li>  (ie: for DIR_FS_SQL_CACHE) <br />

<br /><li><strong>PHP Modules:</strong><br/><ul>
<?
$le = get_loaded_extensions();
foreach($le as $module) {
    print "<LI>$module\n";
}
?>
</ul>
</ul>
<a name="phpinfo"></a>
<h2>PHP Info</h2>Return to the top of the page <a href="#top" title="Back to top"><font style="font-family: Webdings; font-size: 15px; text-decoration: none">5</font></a><?php phpinfo() ?>
<?
	
}
else {
?>
This tool is intended to view your MySQL database status. <br/><br/>
	<form action="<?=$_SERVER['PHP_SELF'];?>" method="post">
	<table border="1" cellpadding="3">
		<tr>
			<td>
				Server Location:
			</td>
			<td>
				<input type="text" name="server">
			</td>
		</tr>
		<tr>
			<td>
				Database Name:
			</td>
			<td>
				<input type="text" name="database">
			</td>
		</tr>
		<tr>
			<td>
				Username:
			</td>
			<td>
				<input type="text" name="username">
			</td>
		</tr>
		<tr>
			<td>
				Password:
			</td>
			<td>
				<input type="password" name="password">
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="submit" value="Retrieve Database Settings">
			</td>
		</tr>
	</table>
	</form>
<?
}
?>
</body>
</html>