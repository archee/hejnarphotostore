<?php
/**
 * Side Box Template
 *
 * @package templateSystem
 * @copyright Copyright 2003-2010 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: tpl_whats_new.php 17617 2010-09-25 20:13:29Z drbyte $
 
 * 'Bask' Template designed by zen-cart-power.net
 * @copyright Copyright 2011 zen-cart-power.net - 'Bask' template
 */
  $content = "";
  $content .= '<div class="sideBoxContent centeredContent">';
  $whats_new_box_counter = 0;    while (!$random_whats_new_sidebox_product->EOF) {
    $whats_new_price = zen_get_products_display_price($random_whats_new_sidebox_product->fields['products_id']);        $imageTag = zen_image(DIR_WS_IMAGES . $random_whats_new_sidebox_product->fields['products_image'], $random_whats_new_sidebox_product->fields['products_name'], 750, 507);    $imageArr = explode("\"", $imageTag);    $imageLarge = $imageArr[1];    
    $content .= "\n" . '  <div class="sideBoxContentItem">';
    $content .= '<a class="jqzoomWhatsNew" href="' . $imageLarge . '" title="" onclick="document.location.href=\''     			 . zen_href_link(zen_get_info_page($random_whats_new_sidebox_product->fields['products_id']), 'cPath='     			 . zen_get_generated_category_path_rev($random_whats_new_sidebox_product->fields['master_categories_id'])     			 . '&products_id=' . $random_whats_new_sidebox_product->fields['products_id']) . '\'">'     			 . zen_image(DIR_WS_IMAGES . $random_whats_new_sidebox_product->fields['products_image'], $random_whats_new_sidebox_product->fields['products_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT);    $content .= '</a><br />'. '<a href="' . zen_href_link(zen_get_info_page($random_whats_new_sidebox_product->fields['products_id']), 'cPath=' . zen_get_generated_category_path_rev($random_whats_new_sidebox_product->fields['master_categories_id']) . '&products_id=' . $random_whats_new_sidebox_product->fields['products_id']) . '">' . $random_whats_new_sidebox_product->fields['products_name'] . '</a>';
    $content .= '<div>' . $whats_new_price . '</div>';
    $content .= '</div>';
    $random_whats_new_sidebox_product->MoveNextRandom();        $whats_new_box_counter++; //Moved counter incrementation to end of loop... makes more sense this way. By default, Zen Cart increments counter at the start of loop.
  }
  $content .= '</div>' . "\n";
