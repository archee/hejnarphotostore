<?php
/**
 * Template Information File
 *
 * @package templateSystem
 * @copyright Copyright 2003-2005 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: template_info.php 2306 2005-11-05 21:34:28Z wilt $
 
 * 'Bask' Template designed by zen-cart-power.net
 * @copyright Copyright 2011 zen-cart-power.net - 'Bask' template
 */
$template_name = 'Bask Black Template';
$template_version = 'Version 1.2';
$template_author = 'zen-cart-power.net team (c) 2011';
$template_description = 'Find more templates at <a href="http://www.zen-cart-power.net">zen-cart-power.net</a>';
$template_screenshot = 'bask_black_template.png';
?>